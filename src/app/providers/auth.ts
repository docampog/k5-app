import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Auth {
  getToken(identityId: string, schoolId: string): Observable<any> {
    const headers = new HttpHeaders({
      'x-api-key': '7JzTRWTuEE2a7kHjGwR0h1tzOxsV7Lh05qIdc2p7'
    });
    const options = { headers };
    const url = `https://12q4v4qo37.execute-api.us-east-1.amazonaws.com/dev/ols/v1/jwt-session`;
    const body = {
      identityId,
      schoolId,
      role: 'student'
    };

    return this.http.post(url, body, options);
  }

  constructor(public http: HttpClient) {}
}
