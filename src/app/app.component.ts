import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private keyboard: Keyboard
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.handleIOSKeyboardOverlap(this.keyboard);
    });
  }

  handleIOSKeyboardOverlap(keyboard) {
    let elementsToHide: HTMLCollectionOf<Element>;
    keyboard.onKeyboardShow().subscribe(() => {
      // @ts-ignore
      let elementsHeight = 0;
      elementsToHide = document.getElementsByClassName('hide-on-keyboard-show');
      if (elementsToHide.length) {
        for (let i = 0, len = elementsToHide.length; i < len; ++i) {
          elementsHeight += elementsToHide[i].clientHeight;
          elementsToHide[i].classList.add('hidden');
        }
      }
    });

    keyboard.onKeyboardHide().subscribe(() => {
      elementsToHide = document.getElementsByClassName('hide-on-keyboard-show');
      for (let i = 0, len = elementsToHide.length; i < len; ++i) {
        elementsToHide[i].classList.remove('hidden');
      }
    });
  }
}
