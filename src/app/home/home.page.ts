import { Auth } from './../providers/auth';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  myurl: string;
  ngForm: FormGroup;

  constructor(
    private authProvider: Auth,
    private iab: InAppBrowser,
    private platform: Platform,
    private keyboard: Keyboard
  ) {
    this.myurl = 'https://d24tjtfptagc83.cloudfront.net/?';
  }

  ngOnInit() {
    this.ngForm = new FormGroup({
      identityId: new FormControl(8801294, Validators.required),
      schoolId: new FormControl(5715, Validators.required)
    });
  }

  login(form: NgForm) {
    this.authProvider
      .getToken(form.value.identityId, form.value.schoolId)
      .subscribe(
        data => {
          console.log(data.token);
          this.openURL(data.token);
        },
        error => {
          console.log(error);
        }
      );
  }
  openURL(token: any): any {
    const inAppBrowserOpts: any = {
      hardwareback: 'no',
      closebuttoncaption: 'x',
      toolbar: 'no',
      location: 'no'
    };
    if (this.platform.is('ios')) {
      inAppBrowserOpts.enableViewportScale = 'yes';
      inAppBrowserOpts.disallowoverscroll = 'yes';
      this.keyboard.hideFormAccessoryBar(false);
    } else if (this.platform.is('android')) {
      inAppBrowserOpts.zoom = 'no';
    }

    console.log(`${this.myurl}token=${token}`);
    const browserInstance = this.iab.create(
      `${this.myurl}token=${token}`,
      '_blank',
      inAppBrowserOpts
    );
  }
}
