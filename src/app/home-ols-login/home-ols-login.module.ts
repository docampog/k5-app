import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeOlsLoginPageRoutingModule } from './home-ols-login-routing.module';

import { HomeOlsLoginPage } from './home-ols-login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeOlsLoginPageRoutingModule
  ],
  declarations: [HomeOlsLoginPage]
})
export class HomeOlsLoginPageModule {}
