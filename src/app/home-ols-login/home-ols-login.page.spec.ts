import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeOlsLoginPage } from './home-ols-login.page';

describe('HomeOlsLoginPage', () => {
  let component: HomeOlsLoginPage;
  let fixture: ComponentFixture<HomeOlsLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeOlsLoginPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeOlsLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
