import { Component, OnInit } from "@angular/core";
import {
  InAppBrowser,
  InAppBrowserOptions
} from "@ionic-native/in-app-browser/ngx";

@Component({
  selector: "app-home-ols-login",
  templateUrl: "./home-ols-login.page.html",
  styleUrls: ["./home-ols-login.page.scss"]
})
export class HomeOlsLoginPage implements OnInit {
  constructor(private inAppBrowser: InAppBrowser) {}

  openWebpage() {
    const options: InAppBrowserOptions = {
      toolbar: "no",
      location: "no",
      zoom: "no"
    };
    const browser = this.inAppBrowser.create(
      "https://test1-login-learn.k12.com/",
      "_self",
      options
    );
  }

  ngOnInit(): void {
    this.openWebpage();
  }
}
