import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeOlsLoginPage } from './home-ols-login.page';

const routes: Routes = [
  {
    path: '',
    component: HomeOlsLoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeOlsLoginPageRoutingModule {}
